# $Header: /var/cvs/mbdyn/mbdyn/mbdyn-1.0/modules/module-hid/Makefile.inc,v 1.2 2014/12/06 17:18:44 masarati Exp $
###############################################################################
#
# Add module dependencies here; e.g., if in dir 'module-X' there's 
# a file 'myfunc.cc' write:
#
# MODULE_DEPENDENCIES=myfunc.o
#
# Then put this file in directory 'module-X' as 'Makefile.inc'
#
###############################################################################

# FIXME: this flag is needed by g++ 4.8 to include <cstdint>
MODULE_INCLUDE="-std=c++0x"

