# $Header: /var/cvs/mbdyn/mbdyn/mbdyn-1.0/modules/module-hydrodynamic_plain_bearing/Makefile.inc,v 1.2 2011/06/19 13:33:43 masarati Exp $
###############################################################################
#
# Add module dependencies here; e.g., if in dir 'module-X' there's 
# a file 'myfunc.cc' write:
#
# MODULE_DEPENDENCIES=myfunc.o
#
# Then put this file in directory 'module-X' as 'Makefile.inc'
#
###############################################################################

MODULE_DEPENDENCIES=diffsizes.lo hydrodynamic_plain_bearing_force.lo hydrodynamic_plain_bearing_force_dv.lo

