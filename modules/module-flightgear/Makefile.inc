# $Header: /var/cvs/mbdyn/mbdyn/mbdyn-1.0/modules/module-flightgear/Makefile.inc,v 1.1 2017/08/25 14:19:26 masarati Exp $
###############################################################################
#
# Add module dependencies here; e.g., if in dir 'module-X' there's
# a file 'myfunc.cc' write:
#
# MODULE_DEPENDENCIES=myfunc.o
#
# Then put this file in directory 'module-X' as 'Makefile.inc'
#
###############################################################################

# FIXME: this flag is needed by g++ 4.8 to include <cstdint>
MODULE_INCLUDE="-std=c++0x"

MODULE_DEPENDENCIES=sendToFlightgear.lo recFromFlightgear.lo
